import { Server } from 'http';

export class StunService {

    public constructor(io: SocketIO.Server) {

        let lastId = 0;
        let users = {};
        let rooms = {};
        io.of('stun').on('connection', socket => {

            let id = ++lastId;
            users[id] = socket;
            socket.emit('joined', id);
            let inRooms = [];

            socket.on('join', data => {
                if (rooms[data.room] == null)
                    rooms[data.room] = { namespaces: { [data.namespace]: [] } };
                else if (rooms[data.room].namespaces[data.namespace] == null)
                    rooms[data.room].namespaces[data.namespace] = [];

                socket.emit('room', {
                    id: data.room,
                    ns: data.namespace,
                    members: rooms[data.room].namespaces[data.namespace]
                });
                rooms[data.room].namespaces[data.namespace].forEach(userId => {
                    users[userId].emit('addToRoom', { room: data, user: id });
                });

                rooms[data.room].namespaces[data.namespace].push(id);
                inRooms.push(rooms[data.room].namespaces[data.namespace]);

                console.log('#' + id + ' joined');
            });

            socket.on('discover', () => {
                for (let user = 1; user <= lastId; ++user)
                    if (user != id && users[user] != null)
                        socket.emit('user', user);
            });

            socket.on('connectWith', data => {
                if (users[data.user]) {
                    users[data.user].emit('newConnection', {
                        user: id,
                        description: data.description
                    });
                } else {
                    console.log(`Could not connect with ${data.user}.`);
                }
            });
            socket.on('accept', data => {
                if (users[data.user]) {
                    users[data.user].emit('accept', {
                        user: id,
                        description: data.description
                    });
                } else {
                    console.log(`Could not accept ${data.user}.`);
                }
            });
            socket.on('addIceCandidate', data => {
                if (users[data.user]) {
                    users[data.user].emit('addIceCandidate', {
                        user: id,
                        candidate: data.candidate
                    });
                } else {
                    console.log(`Could not add ice candidate ${data.user}.`);
                }
            });
            socket.on('disconnect', () => {
                let connectedTo = [];

                inRooms.forEach(room => {
                    for (let user of room)
                        if (connectedTo.indexOf(user) == -1 && user != id)
                            connectedTo.push(user);
                    room.splice(room.indexOf(id), 1);
                });

                for (let user of connectedTo) {
                    if (users[user]) {
                        users[user].emit('remove', {
                            user: id
                        });
                    } else {
                        console.log(`Could not send message from #${id} to #${user} as he is disconnected.`);
                    }
                }

                delete users[id];

                console.log('#' + id + ' left');
            });
        });
    }
}